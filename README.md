# Configuration d'un conteneur ("container")

## Préambule

Nous décrivons dans ce document les étapes à effectuer pour installer et activer
les services suivants sur un conteneur de type linux:

 - serveur web (solution: nginx)
 - module php du serveur web
 - base de données (solution: mariadb)
 - accès à distance (solution: shellinabox)

Les étapes sont volontairement à effectuer à la main pour comprendre le
découpage des différents logiciels et les fichiers de configuration associés.

Elles pourraient aussi être regroupées dans un fichier à l'extension yaml dans
le cadre de déploiement vers des solutions cloud/ en nuage de type Docker ou
Kubernete.

## Etapes

### Création d'un conteneur sur le serveur proxmox de l'école

Etape à réaliser à tour de rôle sur l'interface de l'hyperviseur proxmox avec
l'aide de votre enseignant.

Créez un nouveau conteneur avec comme identifiant votre login eduge (en
retirant juste le début: `edu-`).

Choisissez les paramètres suivants:

`template pour le système d'exploication: debian`

`mémoire: 512 MO`

`espace disque: 5 GO`

`nombre de CPU: 2`

`choisissez DHCP pour la méthode d'assignation d'adresse IP`

`Validez les changement, puis démarrez le conteneur et connectez vous`

Se loguer tout d'abord en local avec l'utilisateur root.

Puis créer un nouvel utilisateur `admin` que vous utiliserez par la suite:

`adduser admin`

Installez le paquet sudo et donnez les droits à l'utilisateur `admin` d'être
administrateur ("super user") de la machine:

`sudo apt install -y

`usermod -a -G sudo admin`

### Accès à distance

`sudo apt install -y shellinabox`

Accédez ensuite à votre conteneur directement depuis un navigateur web à
l'adresse:

`https://MON_SERVEUR_IP:4200`

L'adresse IP de votre serveur peut être obtenue via la commande:

`ip address show dev eth0`

Le nom du périphérique `eth0` peut varier suivant les environnements.

### Installation d'un service web

`sudo apt install -y nginx`

### Installation et activation du support PHP pour le service web

`sudo apt install -y php-fpm`

Activez php en décommentant la section suivante du fichier

`/etc/nginx/sites-available/default` :

`location ~ \.php$ {`

`   include snippets/fastcgi-php.conf;`

`   fastcgi_pass unix:/var/run/php/php7.3-fpm.sock;`

`}`

Les lignes commençant par le caractère `#` sont inactives.

Rechargez ensuite la configuration de nginx via:

`sudo systemctl reload nginx.service`

### Installation du serveur de bases de données

`sudo apt install -y mariadb-server`

### Commandes usuelles pour la gestion des services

Déterminer l'état actuel d'un service:

`systemctl status MON_SERVICE`

Arrêt d'un service:

`systemctl status MON_SERVICE`

Démarrage d'un service:

`systemctl start MON_SERVICE`

Redémarrage (stop+start combiné) d'un service:

`systemctl restart MON_SERVICE`
